---
_fieldset: page
title: About
meta_title: "About Jack Whiting | Jackabox "
meta_desc: "Behind the developer, Jack Whiting. What he does outside of coding."
alt_title: About Jack Whiting
_template: page
---

Hello, I go by the name of Jack Edward Whiting. By day, I can usually be found coding websites, toying around with side projects, or working on some awesome things - fuelled by Yorkshire Tea and boogying along to the music. By night, you will find me catching up on my infinite list of films or playing games on my Xbox or Mac.

I have been developing websites for around eight years in my spare time, however I started working on building up a freelance portfolio in 2011. It has to be one of the best decisions in my life. At the same time, I attended the University of Northumbria, which I graduated in 2014 with a First Class Honours in Web Design & Development.

I lived in Newcastle for 5 years, before moving back to my home territory in East Yorkshire, here I hope to progress my freelance career further and whilst working with **you**, build awesome things!