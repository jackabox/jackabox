---
main_img: "/assets/img/blog/2014/university.jpg"
title: "University Isn't Everything (Part 2)"
summary: "University isn't all it is set up to be, the things you gain should be with regards to personal achievements not just educational. This post surmises my thinking."
meta_title: "University Isn't Everything (Part 2) | Blog | Jackabox"
meta_desc: "University isn't all it is set up to be, the things you gain should be with regards to personal achievements not just educational. This post surmises my thinking."
categories:
  - Personal Thoughts
tags:
  - University
---

Around this time last year I wrote a blog post titled "[University Isn't Everything](http://jackabox.co.uk/blog/university-isnt-everything)" as you can probably guess this is part two. The main point behind the post was to express my thoughts on why university isn't everything anymore and it requires a lot more than a degree to become successful in the web industry and at what you do.

Since the last post, I wrote I finished my second year with a 1st. After a four-month summer break (of which I was mostly ill) I enrolled on my final year to complete my degree.

The main module of the third year is the dissertation. On my course, the dissertation is worth **one-third** of the final grade. It's a great module to allow students to push themselves and pursue something they have a passion in with no restraints on what languages you can use.

I chose to build an application aimed to help people with Crohn's Disease. More info can be found on this at [http://crohnstracker.com](http://crohnstracker.com). I will be building the application with **Backbone.js** for the client and use **Laravel** to create an API to work with the server.

I've never used Backbone before and I'm not the best JavaScript programmer so I wanted to push myself.

### Passion

This was the way I perceive the dissertation. A chance to explore new languages and push my skills. I have also seen very much the opposite attitude to this. I have seen people cower and not be willing to push themselves due to the fear of a poorer grade or no real passion for learning or try something new. The majority of students tend to take the safe option rather than be driven by the fire so many designers and developers have in the "real world".

I attended Industry Conference in 2013 [@iamashley](http://twitter.com/iamashley) said something that I will always remember.

> I had a passion, a passion to try. Passion in my opinion is more powerful then knowledge because passion can lead to that. One person with passion is capable of being more effective then a handful of people with more experience but who don't care enough.
> If you have that wee fire in your belly, and you'll know if you've got it because you can physically feel it. If you have that people will not stand in your way.

*You can watch Ashley's full talk on Vimeo here: [http://vimeo.com/67567032](http://vimeo.com/67567032)*.

Ashley showed the passion which is missing within most university students. The passion to really enjoy and love what you do. I'm not saying that all university students don't have it, there are always people who do but it seems to be the few rather than the many.

It seems to me that the mixture between the modules taught and the lack of enthusiasm causes a bad image for the new entries into the industry.

### Module Content

The modules taught can be relatively mundane. One module at Northumbria University only just beings to introduce HTML5 and jQuery in the final year. The previous two years of taught modules never introduced HTML5, CSS3 coverage was limited, and jQuery was frowned upon.

Things I took for granted within the first few months of teaching myself to code were suddenly danger zones you'd lose marks for. I felt as if I had been dragged 10 steps backwards rather than pushed and excelled. When I was teaching myself I would use Google to try and find a solution to any problems I had, and learn from that solution. I was using HTML5 and jQuery before I even knew I was, but I had to not integrate any of this into University.

Don't get me wrong. If you are a newcomer and don't know anything about coding for the web, or are visual learners, university can be very beneficial. They get to sit down with Tutors and learn modules in a structured manner, limited to that of the module. That is just it though. It's limited to the module, the scope is small, the tutors teach only their module usually and don't merge with that of other modules.

Within five months of me learning to code and building confidence I had launched my first website. This was a custom WordPress theme for a lovely woman who took a chance with me because I showed passion. The website, located at [http://linchpinpa.com](http://linchpinpa.com), involved coding HTML5, CSS3, JavaScript/jQuery, PHP and integrating Google Analytics. It was five months from learning to code to launching a website covering more than that of the first two years of my  degree.

Maybe this was just my University, but it just seems too limited on the scope of what you learn in the time span you are given to learn it.

### Safety Nets
The main benefit university offered me was a safety net. If it hadn't been for University I would not be where I am now.

To provide some background to that. I must have applied for around 100 jobs before turning 18 and I was never successful in getting one of them. At the time of applying this frustrated me, I didn't feel I had any notable skills. I was given interviews but could just never land the job.

I then took a massive leap of faith and rethought the way I looked at the world. If it wasn't going to let me get a job I would create my own. As I mentioned above the first client I managed to successfully land was Linchpin PA. I received this through the connections university provided. It gave me a huge confidence boost and something to show people. After that, I managed to work with lots of amazing people and it got me to where I am now, but the university provided me with my first client.

I would of also never been financially secure enough to do what I do now. University gave me a safety net of **£5,000** a year. Providing me with a roof, money for the bills, and allowed me to buy food so I could live comfortably. With that comfort, I managed build a name for myself. I know a lot of people work with clients on the side of their full-time job before freelancing but for me my full-time job felt like university.

### Closing Thoughts
I have probably mentioned several negative things about University. I don't want it to seem like I think university is a waste of time. University is great for many things: social life, a safety net, close learning with tutors. I just feel like people would be able to do more if only they had the "fire in their belly" to learn more outside of the scope given by universities.

In one sentence, I'd sum up what I'm saying as: **find your passion and embrace it, push the boundaries of what you know and always try learn new things**.

<p style="font-size: 13px; font-style: italic">Header image source: <a href="http://www.flickr.com/photos/brian1970/6776491365/sizes/o">http://www.flickr.com/photos/brian1970/6776491365/sizes/o/</a></p>