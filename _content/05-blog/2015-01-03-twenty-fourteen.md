---
main_img: "/assets/img/blog/2015/twenty-fourteen.jpg"
title: "Twenty Fourteen: A Small Recap"
summary: "2014 has been an interesting year and definitely an eye opening one. I have had a fair few ups and downs, but, overall I am happy everything has worked out the way it has. Below is a small overview of my year and where I wish to take 2015."
meta_title: "Twenty Fourteen: A Small Recap - Jackabox"
meta_desc: "Twenty Fourteen has been an interesting year and definitely an eye opening one. I have had a fair few ups and downs, but, overall I am happy everything has worked out the way it has."
categories:
  - Personal Thoughts
tags:
  - Servers
  - Issues
---

### Work
I worked on several projects that peaked my interest. The first part of 2014 was spent working on **Crohn’s Tracker**, my university dissertation and personal project, this turned out to be a great success for my qualification as I received a First.

However, the second part and majority of the year was spent working for an agency in Newcastle Upon Tyne. I enjoyed the work and was put into a position of being the highest developer in the company, after coming from university that year I felt pretty privileged to be given the responsibility. However, this was not a totally peachy experience and I had to hand my resignation in. I wrote this experience up and it’s available for [further reading](/blog/being-left-ethically-challenged), if you wish.

For 2015, I will be aiming to push my freelancing properly and work on some pretty cool application ideas I have. If all goes well I am hoping to progress to a reliable income and work on a few applications for public use. You will most probably hear some ramblings with regards to this over the coming months.

### Coding / Development
Over the duration of the last year, I managed to really push my skills and learn a lot more about frameworks, servers and workflow management. I’ve started integrating [Gulp](http://gulpjs.com/) and [Bower](http://bower.io/) into my projects. I learnt a lot more about Laravel, with many big thanks to [Laracasts](http://laracasts.com/).

I also learnt a personal lesson of not to underestimate my knowledge. It has been proven to me that I know more then I thought with regards to development and managing a workflow. However until working in the agency and having people look up to me for answers, tips, and hints, I never really understood how people cared for the knowledge I have. For that reason, I want to start writing some more coding tutorials and how-to guides with the hope to help people out in the future.

I aim to learn another coding language, I’m feeling Ruby or to learn a JavaScript framework such as Angular. I’m hoping that learning one, or both, will allow me to create better applications which feel smoother in interaction for the end user. Any tutorials or resources you know of would be awesome!

### Conferences
I never got the opportunity to attend many conferences in 2014, but I did get to attend [DIBI](http://www.dibiconference.com/) for the third year running. Personally I don’t think it really developed me as much as previous years, but it was great to socialise with like minded people and have a recap on Laravel and the way people were using it in the community. The Sharpe team did a pretty good job at handling the conference and [I wrote a round up of DIBI](/blog/overview-of-dibi-2014) which you can feel free to browse for more details.

### Health

My health, with having Crohn’s, has taken a toll on me over the last year, I’ve had a lot of ups and downs with personal dilemmas, stress and anxiety. I only ever really tell people a brief overview of what’s happening with me and my Crohn’s, but it has become a much larger influence on my life than I had ever wanted it to become. I am going to be given some new medication in 2015, and I’ve reduced the sources of stress as much on my life to leave me with a more relaxed experience. The Christmas period, even though busy, has been a total bliss in helping me reset my body.

### 2015 and Beyond

I can’t wait to get started working on some freelance work I have lined up, digging into learning new coding languages, and finally mending my health. I’m hoping to get 2015 kicked off to a rocking start, who knows what is in store for this year! Best wishes to you all.