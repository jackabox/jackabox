---
main_img: ""
title: "Working with Pancake"
summary: "A recap on how I recently got to the opportunity to work with the Pancake team on version 4 of their application."
meta_title: "Working with Pancake | Blog | Jackabox"
meta_desc: "A recap on how I recently got to the opportunity to work with the Pancake team on version 4 of their application."
categories:
  - Design
  - Client Work
---

For those who haven't heard of [Pancake App][Pancake], here's a brief introduction, but I would highly recommend you check it out for yourself too. [Pancake] is a project management and invoicing application that is hosted on your own server, for a one-time fee. The application comes bundled with CRM, Project Management and invoicing features and is aimed to work with freelancers/small teams.

### Joining The Team

Since November 2012 I have been working on the latest version of [Pancake]. I was asked by Lee to update the interface and do some responsive work to add more usability to the system. I have been working on version 4 alongside [Lee], [Zack], [Adam] and [Bruno]. I didn't know it at the time, but this would be the largest and most enjoyable project that I'd have had the chance to work on to date.

### A Little Background

The whole process started, as per usual, with getting my head stuck into Photoshop and pushing around pixels until I had a rough idea of where we wanted to go with the latest version.

With the PSDs ready, it was time to start coding up. Version 4 uses [Foundation] as a base, because it's just awesome. Over the period of a few weeks of coding, with late nights and long days, we managed to get to a point where we had a bunch of new features and the new interface ready for users to start testing.

Early December came and we shipped out a beta, and since then we have shipped many releases working on responsiveness, features and adapting to the users needs. I'm still currently working with the team on the project and constantly pushing out new updates and improvements.

**I'd recommend you to check out [Pancake][v4]**, and no doubt you'll probably hear more about it from me soon. If not, be sure to follow the progress on twitter: [@pancakeapp](http://twitter.com/pancakeapp)

[Pancake]: https://pancakeapp.com/ref/YZN0T4Fg
[v4]: http://pancakeapp.com/version4
[Lee]: https://twitter.com/thatleeguy
[Zack]: http://twitter.com/zackkitzmiller
[Adam]: http://twitter.com/darkhouseca
[Bruno]: http://twitter.com/brunormbarros
[Foundation]: http://foundation.zurb.com/