---
main_img: /assets/img/blog/university.jpg
title: "Get the F* On With it"
summary: "A few lessons I learnt when working on big projects and how you should just get on with it rather then creating obstacles."
meta_title: "Get the F* On With it | Blog | Jackabox"
meta_desc: "A few lessons I learnt when working on big projects and how you should just get on with it rather then creating obstacles."
categories:
  - Personal Thoughts
tags:
  - Work Flow
---

*A small introduction with relevance to the project: I aimed at creating a system which would be pushed live in order to aid people with a chronic condition. I did this off of my own back with no real saving or funds. This is the story of why you should just get on with it and stop putting obstacles in the way.*

### The Project
Around this time last year, I set out to start a project. A project that would aid people with Crohns by helping them meet other users and manage their condition. The project was built in any free time I had and consisted of hundreds of hours pumped into every stage from planning to development, and back again. I spent money on making merchandise, sharing the word of it and setting up the servers ready for the site to go live on. This was not enough though.

### Stop Trying and Do it.
I have come to the realisation over the last few days that the project will struggle to be launched. I built up a small hype for the project of people very interested and then kept missing my custom set deadlines, then setting another and missing that.
What I did not realise at the time is that I was my own downfall. I stood in the way of launching my own project merely because I wanted to perfect it and I wanted to create some so amazing on launch, that's not feasible though.

The deadlines I set myself were great at the time, however they just stood in the way of me focusing my mind on the project at hand. I currently sit on a code basis of a system which has the essentials to do what it was meant to, but not to the standard I foresaw at the launch. I blocked myself off by trying to set unrealistic targets. In actual essence, I should have just built the idea, launched it, and then iterated on the system as it went through a public beta environment.

Each stage I went through over the year left me feeling the project had fizzled more and more. There became less enthusiasm to be seen by the project and much of what I anticipated had become a lot harder to envision and push with the passion I originally set out with.

I set out to hit some investment opportunities in Newcastle and each one I skipped out on due to thinking the project was not a state where anyone would have wanted to invest. I was so wrong on this. I couldn't understand the aspect of it being a development project I was trying to get funding on, I expect to be scrutinised, when in actual fact the investment was so I could work on ruling out the flaws in the development. My mind was my own obstacle in releasing Crohn's Tracker and I will regret it.

__Will it be launched by me? No, which defeats the point of me helping others.__

### A Few Words
Although only a small article, it reflects on the idea that putting so many obstacles in the way can have a negative impact on the project you are working on. In actual fact when you set out on a project you should stop setting unrealistic aims and get on with it. Wasting time will only make your project inevitably fizzle out. Do not skip the steps but do not expect things to have to be perfect when you launch. Take a look at some of the top applications they are released with flaws under the hood and reiterated to be improved.

Go out and do it!