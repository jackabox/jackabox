---
main_img: ""
title: "Being Left Ethically Challenged"
summary: "A series of events occurred which left me challenged in the way that the company runs and put me in a compromising situation. Therefore, I was forced to hand in my resignation."
meta_title: "Being Left Ethically Challenged | Blog | Jackabox"
meta_desc: "Recently I was put in a compromising position. This article covers what happened and how I dealt with this."
categories:
  - Personal Thoughts
tags:
  - Servers
  - Issues
---

### A Bit of Back Story
The company I worked at had a few issues in the past with poor manning of servers and client backups, as in they had __no backups__. This was down to a series of issues both with hardware and staff.

When I got into the company this was noted as an issue and was a warning sign to me we really needed to sort the servers out. With not much experience working in an agency, I played my cards carefully in the first few months to not undermine the staff higher than me, which didn't seem to prioritise the servers as an issue.

This was before it went wrong.

### Where it Got Tricky
The problem that then followed was in relation to the server down and a security breach which left the files and system vulnerable and could be used in malicious activity. There have been a few successful attempts to breach the security of the server, with three phishing scams I had to close up in the past, but this escalated. When the server was closed, the company ultimately lost the whole of their files and databases. Leaving them with a lot of nothings.

It took a little poking and money but by phoning the provider a backup was received, not something that should have had to be done, the company should have had their own backups to push onto a new server or performed the necessary security upgrades. Internally, to those who didn't know, this was an awesome moment - we had the files back. However, the backup was from 12 hours before the server was closed, meaning they all files were potentially corrupt.

### What the Company Decided
We sat down for a long meeting and was addressed that we had to put all of these sites live, without checking them for any security issues, and hope - yes *hope* - that there were to be no repercussions. I was informed by a higher body in the company that it was fine and that if anything came back he would take responsibility.

I couldn't quite comprehend this - being told that it was fine to put potentially corrupt sites live which processed individuals/businesses details and took card payments. I, by no means, am an expert on security, but even this seems like an illogical decision to make for a company.

A lot of back and forth went on within the company over discussion of what to do, but my opinion was ultimately disregarded. I was then told it was either put the sites live or there was no company and effectively everyone out of a job, including a woman who was well into her pregnancy.

### Having to Make a Decision
I had to decide what to do, I was left with a choice, forced on me, to either be part of putting sites live with potentially corrupt data or to do nothing and have my colleagues jobs on my hands. The fact I was made to do this put me under a lot of stress, it didn’t give me much choice and it didn't feel like I had much room to maneuverer.

The stress led to me being ill, the being ill forced me to not go back into work, and not working meant I lost my wages for the whole week. The circumstances not only gave me a choice to make, but made me so ill I had to take the time off work and lost out on a weeks wage, which is a lot for me.

### What Happened Next
I spent the week, whilst ill, deciding what to do and where to go from here. No matter what happened with the server and if the company could get a clean copy of the sites live I would not feel comfortable at the company. The way I was backed into a corner, and the total lack of security and internal management, and the way my health was put under pressure all pushed me to resign.

I felt I had no choice but to leave the company, no choice but to move on.

All I can say is lessons are learnt and this will make me a lot weary when working within companies. Either way I am now left seeking freelance work or recruitment in Leeds.

### A Simple Message
Please make sure to backup your client sites regularly, please keep clean backups safe, please make sure you aren't forced into a position where your job is either on the line or doing something unethical. Always prepare for the worst, rather than trying to stick the bits back together again after a catastrophe.
