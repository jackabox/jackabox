---
main_img: ""
title: "Struggling with Crohn's Disease"
summary: "A small article written around the struggles with Crohn's Disease whilst working in a Web Design agency."
meta_title: "Struggling with Crohn's Disease | Blog | Jackabox"
meta_desc: "A small article written around the struggles with Crohn's Disease whilst working in a Web Design agency."
categories:
  - Personal Thoughts
tags:
  - "Crohn's Disease"
---

_A small vent around struggling with Crohn's Disease and how it bites back at the worst times._

### Then
I've been diagnosed with Crohn's Disease for around four years now. I have talked about it several times through an array of outlets. I wouldn't by any means say I've had the worst time, compared to others, but it sure has not been pleasant.

I used to freelance whilst I was at university and this meant that I could, relatively, live my life around my Crohns when I didn't feel well I would hibernate, when I felt okay I would work, sometimes into the early hours of the next day. I always got my work done and could manage it because if I had to I would pull long hours, work weekends or control my inflow of work to accommodate.

### Now
Come July 2014, I decided to take a job in an agency. I wanted to gain more experience working with clients, get guidance off people who've been in the industry for longer, and work a steady pattern. I felt like if I could work that nine to five job, then I would be in a routine and my Crohns would fall into line, this is not what happened.

As I started my job one or two months went by without a hitch. I settled in, started building websites, working on some decent projects, got a promotion and was well underway to fitting in nicely. Then, my Crohns decided to fight back.

I was in the hospital for a check-up after feeling rough, nauseated and fatigued. My doctor informed me that it was time to come off the medication that had been holding me together for two years, as this was the supposable cause of the pain.

I came off them unwillingly and have been with no medication for three weeks, and counting, but this just seems to have made me feel three times. I'm struggling to get into work putting my job on the line, be happy and generally concentrate. I was informed I would get an 'emergency' MRI scan to check why my Crohns was flaring up, of which I've seen no further confirmation, and no medication given to help me fight down the Crohns.

### Summary
Fighting with Crohns daily makes it a struggle to wake up and smile every day. It is something that will just burden me and others. I put my thoughts out to every single person who is struggling with chronic pain, whatever the source of it.

*Vent over.*