---
main_img: ""
title: "Fade in Anchor Button with jQuery"
summary: "A quick tutorial to teach you how to implement an anchor which brings the user back to the top of the page."
meta_title: "Fade in Anchor Button with jQuery | Blog | Jackabox"
meta_desc: "A quick tutorial to teach you how to implement an anchor which brings the user back to the top of the page."
categories:
  - Tutorials
  - Snippets
tags:
  - jQuery
---

Today I am going to show you how to set up some code so that when you scroll down the page, past a certain point, an element will fade in and be present and fade out when you scroll up the page. This is useful for when you want to display a "TOP" link after you have scrolled down a certain amount.

### 1. The HTML

Firstly let's set up the HTML. We need to set up the area we want to link to, then set up the link. For this tutorial we will use the body as our destination, to do this all we need to do is add an id to the tag, let us call this "top" as it will be the top of the page.

```.language-php
<body id="top">
```

Now we need to set up the div where we will contain our link. Let us give the div an id of toTop as we will need to call this in the jQuery. Just put the following code anywhere on the page, preferably the top or bottom of the body tag (before JavaScript).

```.language-php
<div id="toTop">
  <a href="#body" title="Back to the top">Top</a>
</div>
```

That's all the HTML we need to add to our files, now let's go onto the CSS.

### 2. The CSS

We need to make the div fixed in it's position so that when you scroll down the page it stays in the bottom right (this is typical of sites and is the place to put the top button, instead of the left hand side). So to style the link to the right and fixed we need to add the following CSS. By default, we must also have the div as hidden so that it doesn't show up until we scroll.

```.language-css
#toTop {
  position: fixed; /* Set the position to fixed */
  bottom: 20px; /* Move the div 20px from the bottom */
  right: 50%; /* Move the div 50% from the right */
  margin-right: -500px; /* Move the div back by %px (usually half the width of the div) */
  display: none; /* set to display none initially */
}
```

### 3. The jQuery
To get the functions to work properly we will need to test when we scroll from the top and then fadeIn the div we set as <strong>"display: none;"</strong>. Note we will need an else case where if we scroll back up, before the width we set, we need to fade the div out again. The reason we do this is because we don't need to display the anchor when we have the top visible already.

```.language-javascript
$(window).scroll( function(){
  if($(window).scrollTop()>200){
    $("#toTop").fadeIn();
  } else {
    $("#toTop").fadeOut();
  }
});
```

Note: Don't forget to call the jQuery library in somewhere!