---
main_img: ""
title: "Toggle a Search Box with jQuery"
summary: "A quick tutorial to teach you how to get a search box to show and hide using jQuery without much effort."
meta_title: "Simple Toggling Search Box with jQuery | Blog | Jackabox"
meta_desc: "A quick tutorial to teach you how to get a search box to show and hide using jQuery without much effort."
categories:
  - Tutorials
  - Snippets
tags:
  - jQuery
---

This quick and easy tutorial will teach you how to set up and toggle a search box with jQuery so that upon clicking a link the search box input field will appear if hidden, and vice versa. This is quick tutorial will only take a few moments to set up and utilises the built in search features of WordPress. A demo of what we are going to achieve can be seen on this site, click the search button in the top right corner.

### 1. Setting Up The Search Box

For this we need to create a small portion of HTML wherever we want the search box to appear. The following code is how I've set mine up.

```.language-php
<div id="sarea">
	<form id="searchform" action="<php echo home_url( '/' ); >" method="get" role="search">
		<input id="s" type="text" name="s" value="" placeholder="Search Here & Hit Enter">
	</form>
</div>

<a href="#" id="sbutton"> Search </a>
```

The code above uses two IDs; the first is for the search area (id = "sarea") and the second is for the search button (id = "sbutton"). Wrapped inside of the sarea div there is a form with an id of "s" and a href of `<?php echo home_url ( ' / ' ); ?>` - by convention we search by directing the form to the home page. This will load the search the database and pull back appropriate data.

### 2. The CSS

When we load up the page we want the sarea to be hidden by default. We can use a simple CSS property none as visibility to do this. Below is the snippet of code which needs attaching to the sarea along with any other styling you may have.

```.language-css
#sarea {
	visibility: hidden;
}
```

### 3. Using jQuery To Toggle

Now for this part of the tutorial we need to set up the jQuery to check for if the sbutton element is clicked and then when it is perform a function which will toggle the visibility of the sarea. We can do that with the following code below.

```.language-javascript
$("#sbutton").click(function () {
	$('#sarea').toggle();
});
```

Therefore if the sarea is visible it will hide it and if it's hidden it will appear. Don't forget to load the latest jQuery and put the code at the bottom of your page to optimise page load speed.

All done. You should now have the form element hiding and showing, if you have any problems do let me know.