---
main_img: ""
title: "A Small Update on Crohn's tracker"
summary: "A small update on the progress of developing my application for people with Crohn's Disease."
meta_title: "A Small Update on Crohn's Tracker | Blog | Jackabox"
meta_desc: "A small update on the progress of developing my application for people with Crohn's Disease."
categories:
  - Development
tags:
  - "Crohn's Tracker"
---

For the past two/three months I have been planning away at my latest project - Crohn's Tracker - and it has really left me with a great feeling. The application is designed to fill a gap in the market, for those with Crohn's Disease, Colitis, and IBD, by helping them manage their condition in once place.

The application is taking shape now through both the internal and external elements. The new landing page is live now and will be updated with a couple of new sections soon. The internal elements are building up on the profile to really outline the features I think you are going to love.

I am looking to let the first lot of users sign up for early access in **September** and cannot wait to get people involved. I've set up a twitter ([@crohnstracker](http://twitter.com/crohnstracker)) to raise awareness in the application as I feel we are now at that point where we can get people involved and let them know what's coming soon! The feedback received so far seems positive towards the application and that it will definitely help people out. I will keep you updated on the development via here or the official website at [http://crohnstracker.com](http://crohnstracker.com).