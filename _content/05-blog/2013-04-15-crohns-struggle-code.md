---
main_img: ""
title: "Crohns, Struggle & Code"
summary: "My dealings with Crohn's Disease and how I've struggled over the last year, how I've got over it, and how I've managed to push to be stronger."
meta_title: "Crohns, Struggle & Code | Blog | Jackabox"
meta_desc: "My dealings with Crohn's Disease and how I've struggled over the last year, how I've got over it, and how I've managed to push to be stronger."
categories:
  - Personal Thoughts
tags:
  - "Crohn's Disease"
---

I have not really publicly let people in to know about the last 8 months, or so, of my life. I've been struggling quite a bit recently and more so at the current date. Why? I was officially diagnosed in August with Crohn's Disease. Crohn's is a chronic IBD (inflammatory bowel disease), it's something I'm going to have to live with for the rest of my life. It affects about 1 in every 1200 people so I know I'm not alone with it, in fact maybe you have it too.

I've been trying to work hard for the last year on building up a freelance business to set myself up when I finish university, this was going well until I got struck by a series of bad news commencing from Christmas onwards. Firstly I found a medication that worked for a few weeks, but then it didn't, it caused me to have problems with my liver which forced me to be taken off it and move onto a more aggressive medication named Methotrexate. Methotrexate is a form of immunosuppressant and in large doses used for chemotherapy. It carries a lot of side effects and honestly, I'm not coping greatly on it.

I've been so worn down over the last few weeks with things getting on top of me one after another. But on top of all that I'm trying to not let it get me down, sometimes that is harder said than done! I've been building up a collection of diaries and notes on my struggles, and with writing them I've always wondered how other crohnies (a term for others with Crohns) dealt with it.

### The Future
I have always wanted someone to help me and guide me through stages, but I don't know anyone close with it. This has made me want people with Crohn's to get as much help as possible. There are forums where you can get in touch, and some communities around for people to communicate in focus groups, but I wanted to create something that expands on this.

Over the summer I plan to start developing an application that will build as a place where people can track their symptoms, issues, medication, create a diary and much more. This will, hopefully, be the place where people can turn to and share their journey with others who also have Crohn's. It's under development at the moment so I've set up a mailing list at [http://crohnstracker.com][1] where you can sign up for more details.

This is only a little bit of me opening up to the public, but I'm ready to tackle it all head on. Any thoughts? I'd love to know.

[1]:	http://crohnstracker.com "Crohn's Tracker"