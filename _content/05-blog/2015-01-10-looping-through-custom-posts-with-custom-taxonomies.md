---
main_img: ""
title: "Looping through custom posts by custom taxonomies in WordPress"
summary: "WordPress snippet to allow you to loop around a custom post's taxonomies and then query out the posts for that category."
meta_title: "Looping through custom posts by custom taxonomies in WordPress | Blog | Jackabox"
meta_desc: "WordPress snippet to allow you to loop around a custom post's taxonomies and then query out the posts for that category."
categories:
  - Tutorials
  - Snippets
tags:
  - WordPress
  - PHP
---

I was asked to create a website recently, and I needed to allow as much customisation as possible. The site had a section of services, categorised, which needed to be looped through and filtered. The below code was the way I approached it and proved effective, therefore, I figured I’d share it with whoever found themselves in this position. The code is as follows:

``` .language-php
<?php
// Get all the categories
$categories = get_terms( 'service-category' );

// Loop through all the returned terms
foreach ( $categories as $category ):

    // set up a new query for each category, pulling in related posts.
    $services = new WP_Query(
        array(
            'post_type' => 'services',
            'showposts' => -1,
            'tax_query' => array(
                array(
                    'taxonomy'  => 'service-category',
                    'terms'     => array( $category->slug ),
                    'field'     => 'slug'
                )
            )
        )
    );
?>

<h3><?php echo $category->name; ?></h3>
<ul>
<?php while ($services->have_posts()) : $services->the_post(); ?>
    <li><?php the_title(); ?></li>
<?php endwhile; ?>
</ul>

<?php
    // Reset things, for good measure
    $services = null;
    wp_reset_postdata();

// end the loop
endforeach;
?>
```

### Breaking Down the Code

Let us break down the code a little to see what we are working with. The first thing, we need to do, is get all the taxonomies from the custom category we set up. In my case, this was *service-category*.


``` .language-php
// Get all the categories
$categories = get_terms( 'service-category' );
```

Once we have got those taxonomies, we are going to loop around them and set up a query for each taxonomy, in the post type our taxonomy belongs to. We do this by grabbing the slug attached to the category and then querying it via a *tax_query*.

``` .language-php
// Loop through all the returned terms
foreach ( $categories as $category ):
    $services = new WP_Query(
        array(
            'post_type' => 'services',
            'showposts' => -1,
            'tax_query' => array(
                array(
                    'taxonomy'  => 'service-category',
                    'terms'     => array( $category->slug ),
                    'field'     => 'slug'
                )
            )
        )
    );
```

The next part of the code is merely to echo out what we want from each post. Using the query and while loop we are allowed to tie into WordPress's functions within the loop (i.e. postdata which includes;  *the_title, the_content, etc*). Don't forget we are also in the foreach here for $category so we can also pull any fields related to that.

```.language-php
<h3><?php echo $category->name; ?></h3>
<ul>
<?php while ($services->have_posts()) : $services->the_post(); ?>
   <li><?php the_title(); ?></li>
<?php endwhile; ?>
</ul>
```

Finally we need to tidy up the code, we will clean the query, reset any postdata and ensure when we next loop round that we don't have any redundant data. Finally, we close up the foreach we started.

``` .language-php
    // Reset things, for good measure
    $services = null;
    wp_reset_postdata();
endforeach;
```

###Conclusion

This is only a small part but utilises custom taxonomies with custom post types and allows you to loop around them grouped by taxonomy. Hope that helps you out. Feel free to discuss below.