---
main_img: ""
title: "New Year, New Site"
summary: "With the New Year hitting, I've undertaken a new build for my website using a totally new CMS."
meta_title: "New Year, New Site | Blog | Jackabox"
meta_desc: "With the New Year hitting, I've undertaken a new build for my website using a totally new CMS."
categories:
  - Development
tags:
  - Statamic
---

As of writing this post I'm half way through my University life. Over this time I would like to think I have learnt an incredible amount about the web industry and myself. The various books I've read, people I've spoken to, the lessons I've learnt and the lessons I have been taught have helped me to improve who I am as a person and as a designer.

### Building Up To This
I decided towards the end of last year that it was time to rebrand and refresh my website under the working name of "Jackabox". You may wonder why I chose that name but it was quite a simple decision, it is easier to remember and catchier than my full name "Jack Whiting". That being said, the branding is not 100% there yet and I will probably put it, and this site, through several revisions before I am fully happy - but what is design if not a constant improvement!

### Creating The Site
The process of creating this site was enjoyable for me. Why? Well it started when I was informed of a CMS named [Statamic](http://statamic.com). Now [Statamic](http://statamic.com) is a much more lightweight CMS compared to my usual, [WordPress](http://wordpress.org), because it uses a flat file system built around my markdown files. I still love [WordPress](http://wordpress.org) but when I could embrace [Statamic](http://statamic.com) to run my system on, without the need for all the bloat, I couldn't resist.

Once I made the leap and got hold of a license I couldn't wait to get stuck in. I spent hours looking through the system and learning the ins and outs as best I could at the time. It wasn't until I actually started to build my site that I realised how wonderfully pleasant it was to work with, things were so straight forward that I over thought them and kicked myself when I was pointed in the right direction.

### It's Finally Here
After a few weeks of designing, coding, and testing, I'm happy to say the site is finally here and live! I still love [Statamic](http://statamic.com) as much as when I started out with using it just a few weeks ago, and would recommend it to all those looking for a lightweight solution which renders flat files in the web, without the need to compile.

I hope you enjoy my new site, please feel free to take a look around. Happy New Year!