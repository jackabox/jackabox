---
main_img: "/assets/img/blog/2014/university.jpg"
title: "University Isn't Everything (Part 1)"
summary: "University isn't all it is set up to be, the things you get from it should be highly around personal achievements not just educational. This post surmises that concept."
meta_title: "University Isn't Everything (Part 1) | Blog | Jackabox"
meta_desc: "University isn't all it is set up to be, the things you get from it should be highly around personal achievements not just educational."
categories:
  - Personal Thoughts
tags:
  - University
---

I've been brought up to think that university is a key part to getting anywhere substantial. Do not get me wrong, there is a lot you can get out of it, but I do not think it is all it is cracked up to be, or even *that* important.

When I joined the university I chose to study **Mathematics & Business**, and at that point, taught education was highly important to me. Everything I had been taught and learnt was in a traditional manner. I studied hard to get into university and was very proud when I got in.

Whilst studying Mathematics & Business it began to feel like it was not a good fit for me, and with having a strong background in education, I felt the only right thing to do was to find a new course which would better fit me.

I pondered for a few weeks about what course to change to and in the end decided that **Web Design & Development** would be a good option. I always enjoyed it as a hobby but never saw it as a potential for a career until last year. I secured a few clients, and that is when everything seemed brighter for me. However, I learnt more from working with clients in a few months than the education my university had provided me with to date. I have gotten to a point now, 1.5 years in, where I feel I am ready to start working freelance full time, rather than the part time I do at the moment.

Do not misunderstand me, university is great for those who need a base to learn and some guidance on where to start, but the choice to partake in university should never be taken so easily. I made the decision because that is what I have grown up to believe and know. I am happy being here, but I just do not feel challenged enough for it to be worth the £36,000 debt I will leave with by 2014.

If I could provide you with **one bit of advice**, it would be to speak to people who’ve been through university, find out how much of a necessity it is to have a degree, and really think about the benefits of being taught in the educational system.

I would love to hear your thoughts on the matter and if anyone else has had a similar experience.