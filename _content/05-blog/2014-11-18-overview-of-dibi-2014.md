---
main_img: ""
title: "Overview of DIBI 2014"
summary: "An overview of the DIBI 2014 conference - a two-day web design and development conference based in Newcastle, UK."
meta_title: "Overview of DIBI 2014 | Blog | Jackabox"
meta_desc: "An overview of the DIBI 2014 conference - a two-day web design and development conference based in Newcastle, UK."
categories:
  - Reviews
  - Conferences
tags:
  - DIBI
---

Between 10th and 11th November I was fortunate to attend [DIBI 2014](http://www.dibiconference.com/), a two-day conference around the web industry based in Newcastle, UK. Whilst here I decide to attend the Laravel workshop and pop in on most of the talks.

### Day 1: Workshops
There were four workshops available this year, based around the following topics: Debugging, UX, Laravel and Drupal. The obvious choice for me was to attend the Laravel talk based around the upcoming version (5). The workshop was run by Michael Peacock ([@michaelpeacock](https://twitter.com/michaelpeacock)). He provided a good basis on the basics of Laravel 5 and worked his way up to a few intermediate elements.

The aim was to build an Event Booking application from the start. Information was given to explain the fundamentals and structure of Laravel. Explanations were given to the changes which have occurred. After the analysis of the folder structure for Laravel, the workshop went on to dig into the code. Setting up environments, using Artisan (the command line interface for Laravel), covering migrations, middleware and more.

The overall information provided was easy to follow for people who didn't know much about Laravel and those who use it on a regular basis. Peacock provided a solid point of reference and answered any questions effectively, showing his knowledge for the framework.

### Day 2: Talks
The way the conference had been structured was unlike the original concept to DIBI, but more a push to try inspire you in your career with a few basic tips here and there. The talks which occurred were more focused on inspirational talks and basic guides. In 2012 when I attended DIBI it was a two track conference with paths for Design and Development.

My favourite talk of the day would have to be Dan Rubin's. The talk he provided was pretty enlightening, talking about how people's products are not magical and that there is a major hype towards telling everyone how awesome your product is, when you have not tested it, and it might not be awesome to the user, but to you it is. It was a talk which tried to make you think about the way in which you advertise yourself, your products and just to get on with it and if through that you help someone, great! On a side note check out Dan Rubin's twitter here: [@danrubin](https://twitter.com/danrubin).

There were several other talks throughout the day, but none of them seemed to lodge into my head and stay with me once I had left, due to seeming either too basic or not for my personal taste.

### Overall
I'd say that the Sharpe team ran the conference well, however it didn't feel like what I was expecting. I was hoping for a more focused day of talks on processes with coding, tips, tricks and all that jazz. If the conference carries on this way I do feel they should rename it from DIBI to another name, and I wish them the best of luck in their future endeavours.