---
main_img: ""
title: "Picking up Laravel"
summary: "I recently pushed to use the Laravel framework. This blog post surmises a bit of my learning and why it is so awesome."
meta_title: "Picking up Laravel | Blog | Jackabox"
meta_desc: "I recently pushed to use the Laravel framework. This blog post surmises a bit of my learning and why it is so awesome."
categories:
  - Development
tags:
  - Laravel
---

> Laravel is a clean and classy framework for PHP web development. Freeing you from spaghetti code, Laravel helps you create wonderful applications using simple, expressive syntax. Development should be a creative experience that you enjoy, not something that is painful. Enjoy the fresh air.

How very true this statement is. I picked up **[Laravel]** last weekend to try and clean up the way I was writing code for a University project. I watched a few demonstration videos on how Laravel was used and began to fall in love with how easy it was to understand. The code that I produce is so readable it's almost as if I was reading Pseudo code or the notes I would make before diving into a task.

Over the last week I've read through the documentation provided by [Laravel] and looked at how certain functions work, this has allowed me to build an understanding and then get stuck in.

I picked up the latest stable version of Laravel and started hacking at the code. I started by creating an authentication system, allowing me to check when an admin was logged in. I then went on to build the user system and allow the creation of new users and editing of the older users. The **RESTful** elements are wonderful and improve the way I write my controllers.

The system I'm building has been increasing in complexity and provided several challenges, which I've been able to overcome with the great documentation and community wrapped around the framework, however I can only say happy things about Laravel and look forward to completing my first application with it. I'll be sure to let you see what I've created.

**Update on 7th July 2013:** I've now started working with version four on my latest application of Crohn's Tracker. Which I hope to launch the Beta of in the next coming month.

You can take a look at their website for information about the framework found at: [http://laravel.com][laravel].

[laravel]: http://laravel.com