---
_fieldset: page
title: Home
meta_title: Jackabox - Freelance Web Developer in East Yorkshire
meta_desc: "The works of Jack Whiting, a freelance web developer in East Yorkshire, England. Working on small websites, ecommerce and bespoke builds."
alt_title: Jackabox
_template: front
_layout: front
---
