/* 
	 00. GUMBY SETTING
	 01. LAZYLOAD SETTING
	 03. SLIDER SETTING
	 04. SCROLLTO SETTING
	 05. ISOTOPE PROJECTS
	 06. PROJECTS PORTFOLIO HOVER
*/

// Gumby is ready to go
Gumby.ready(function() {
	console.log('Gumby is ready to go...', Gumby.debug());

	// placeholder polyfil
	if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
		$('input, textarea').placeholder();
	}
});

// Oldie document loaded
Gumby.oldie(function() {

});

/*-----------------------------------------------------------------------------------*/
/*	01. LAZYLOAD SETTING
/*-----------------------------------------------------------------------------------*/
	
$(function() {
    if(document.documentElement.clientWidth > 640) {
        $("img").lazyload({
            effect: "fadeIn"  
        });
    };
});


/*-----------------------------------------------------------------------------------*/
/*	03. SLIDER SETTING
/*-----------------------------------------------------------------------------------


$(document).ready(function(){
    var options = {
        nextButton: true,
        prevButton: true,
        pagination: false,
        animateStartingFrameIn: true,
        autoPlay: true,
        autoPlayDelay: 3000,
        preloader: true,
        preloadTheseFrames: [1],
    };
    
    var mySequence = $("#sequence").sequence(options).data("sequence");
});	*/

/*-----------------------------------------------------------------------------------*/
/*  04. SCROLL TO SETTING
/*-----------------------------------------------------------------------------------*/

$('#nav a.stop').click(function(e){
    $('html,body').scrollTo(this.hash, this.hash);
    e.preventDefault();
});

/*-----------------------------------------------------------------------------------*/
/*	05. ISOTOPE PROJECTS
/*-----------------------------------------------------------------------------------*/
$(document).ready(function () {
    var $container = $('#projects_grid .items');
    $container.imagesLoaded(function () {
        $container.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows'
        });
    });

    $('.filter li a').click(function () {

        $('.filter li a').removeClass('active');
        $(this).addClass('active');

        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });

        return false;
    });
});

/*-----------------------------------------------------------------------------------*/
/*	06. PROJECTS PORTFOLIO HOVER
/*-----------------------------------------------------------------------------------*/
$(function () {
    $(' .items > li, .frame > a ').each(function () {
        $(this).hoverdir();
    });
});