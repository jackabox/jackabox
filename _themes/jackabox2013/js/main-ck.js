/* 
	 00. GUMBY SETTING
	 01. LAZYLOAD SETTING
	 03. SLIDER SETTING
	 04. SCROLLTO SETTING
	 05. ISOTOPE PROJECTS
	 06. PROJECTS PORTFOLIO HOVER
*/// Gumby is ready to go
Gumby.ready(function(){console.log("Gumby is ready to go...",Gumby.debug());(Gumby.isOldie||Gumby.$dom.find("html").hasClass("ie9"))&&$("input, textarea").placeholder()});Gumby.oldie(function(){});$(function(){document.documentElement.clientWidth>640&&$("img").lazyload({effect:"fadeIn"})});$("#nav a.stop").click(function(e){$("html,body").scrollTo(this.hash,this.hash);e.preventDefault()});$(document).ready(function(){var e=$("#projects_grid .items");e.imagesLoaded(function(){e.isotope({itemSelector:".item",layoutMode:"fitRows"})});$(".filter li a").click(function(){$(".filter li a").removeClass("active");$(this).addClass("active");var t=$(this).attr("data-filter");e.isotope({filter:t});return!1})});$(function(){$(" .items > li, .frame > a ").each(function(){$(this).hoverdir()})});