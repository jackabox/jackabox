/*!
 * jquery.scrollto.js 0.0.1 - https://github.com/yckart/jquery.scrollto.js
 * Scroll smooth to any element in your DOM.
 *
 * Copyright (c) 2012 Yannick Albert (http://yckart.com)
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 * 2013/02/17
 **/$.scrollTo=$.fn.scrollTo=function(e,t,n){if(this instanceof $){n=$.extend({},{gap:{x:0,y:0},animation:{easing:"swing",duration:600,complete:$.noop,step:$.noop}},n);return this.each(function(){var r=$(this);r.stop().animate({scrollLeft:isNaN(Number(e))?$(t).offset().left+n.gap.x:e,scrollTop:isNaN(Number(t))?$(t).offset().top+n.gap.y:t},n.animation)})}return $.fn.scrollTo.apply($("html, body"),arguments)};