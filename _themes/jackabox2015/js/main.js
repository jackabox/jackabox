/*-----------------------------------------------------------------------------------*/
/*  SCROLL TO SETTING
/*-----------------------------------------------------------------------------------*/

$('.stop').click(function(e){
    var pos = $(this.hash).offset().top - 80;
    $('html,body').scrollTo(pos, pos);

    $('.site__nav a.current').removeClass('current');
    $(this).addClass('current');

    e.preventDefault();
});

/* If we link to a hash, jump to that position with scrollTo, smoothly. */
if(window.location.hash) {
    pos = $(window.location.hash).offset().top - 80;
    $('html,body').scrollTo(pos, pos);
}


/*-----------------------------------------------------------------------------------*/
/*  Fix header to top
/*-----------------------------------------------------------------------------------*/

var distance = $('.site__header').offset().top;

$(window).scroll(function () {
    if ( $(window).scrollTop() >= distance ) {
        $('.site__header').addClass('scrolled');
        $('.site__header--antijump').show();
    } else {
        $('.site__header').removeClass('scrolled');
        $('.site__header--antijump').hide();
    }
});

// Check for position on load
if ( $(window).scrollTop() >= distance ) {
    $('.site__header').addClass('scrolled');
    $('.site__header--antijump').show();
}


/*-----------------------------------------------------------------------------------*/
/*  ISOTOPE PROJECTS
/*-----------------------------------------------------------------------------------*/
$(document).ready(function () {
    var $container = $('.projects__grid');
    $container.imagesLoaded(function () {
        $container.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows'
        });
    });

    $('.filter li a').click(function () {

        $('.filter li a').removeClass('active');
        $(this).addClass('active');

        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });

        return false;
    });
});

/*-----------------------------------------------------------------------------------*/
/*  PROJECTS PORTFOLIO HOVER
/*-----------------------------------------------------------------------------------*/
$(function () {
    $('.projects__grid li a').each(function () {
        $(this).hoverdir();
    });
});


/*-----------------------------------------------------------------------------------*/
/*  PROJECTS PORTFOLIO HOVER
/*-----------------------------------------------------------------------------------*/
$(function(){
    $('.site__nav ul').slicknav({
        'label': ''
    });
});